1. LIST THE BOOKS AUTHORED BY MARJORIE GREEN(213-46-8915) 

	- BU1032 The Busy Executive's Database Guide
	- BU2075 You can Combat Computer Stress!

2. LIST THE BOOKS AUTHORED BY MICHAEL O'LEARY(267-41-2394)

	- BU1111 Cooking with Computers
	
3. WRITE THE AUTHOR/S OF "THE BUSY EXECUTIVES DATABASE GUIDE" (BU1032)

	- 213-46-8915 Marjorie Green 
	- 409-56-7008 Abraham Bennet

4. IDENTIFY THE PUBLISHER OF "BUT IS IT USER FRIENDLY?" (PC1035)

- 1389 Algodata Infosystems

5. LIST THE BOOKS PUBLISHED BY ALGODATA INFOSYSTEMS(1389)

	- The Busy Executive's Database Guide
	- Cooking with Computers
	- Straight Talk About Computers
	- But Is It User Friendly?
	- Secrets fo Silicon Valley
	- Net Etiquette